package marionettes;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class UIPanel extends JPanel implements MouseListener, ComponentListener {
	private static final long serialVersionUID = 1L;

	int panelHeight = 300;
	int panelWidth;
	String[] puppetNames;
	int puppetNumber = 0;
	int currentEmote = 0;
	UI ui;
	String currentPuppet;
	ArrayList<Integer> hats = new ArrayList<Integer>();
	ShowPanel showPanel;

	public UIPanel(ShowPanel showPanel) {
		this.showPanel = showPanel;
		ui = new UI(showPanel);
	}

	/**
	 * Paints things.
	 */
	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		Graphics2D g2 = (Graphics2D) g;
		if (puppetNumber == 0 || currentPuppet == null) {

		} else {
			ui.initUI(panelHeight, panelWidth, g2, puppetNames, puppetNumber,
					currentPuppet);
		}
	}

	/**
	 * Sets the current puppet and repaints the ui.
	 * 
	 * @param name
	 * @param fromShowPanel
	 */
	public void setCurrentPuppet(String name, boolean fromShowPanel) {

		int currentPuppetNum =indexOf(puppetNames,name);
		this.currentEmote=0;
		showPanel.setCurrentEmote(0);
		this.currentPuppet = name;
		System.out.println(name);

		repaint();
		if (!fromShowPanel) {

			showPanel.setCurrentChar(name, currentPuppetNum);
		}
	}

	public int indexOf(String[] array, String value) {
		for (int i = 0; i < array.length; i++)
			if (array[i] == value)
				return i;
		return -1;
	}
	/**
	 * Sets the current emote and repaints the ui.
	 * 
	 * @param emote
	 */
	public void setCurrentEmote(int emote) {
		this.currentEmote = emote;
		showPanel.setCurrentEmote(emote);
		repaint();
	}

	public int getCurrentEmote() {
		return currentEmote;
	}

	/**
	 * Returns the current puppet.
	 * 
	 * @return
	 */
	public String getCurrentPuppet() {
		return currentPuppet;
	}

	public ArrayList<Integer> getCurrentHats() {
		return hats;
	}

	public String getCurrentHatsString() {
		String hatString = "";
		for (int i = 0; i < hats.size(); i++) {
			hatString += hats.get(i) + ",";
		}
		return hatString;
	}

	/**
	 * Sets the array of puppet names and repaints the ui.
	 * 
	 * @param puppetNames
	 */
	public void setPuppetNames(String[] puppetNames) {
		this.puppetNames = puppetNames;
		puppetNumber = puppetNames.length;
		repaint();
	}

	/**
	 * Checks for mouse clicks and changes things accordingly.
	 */
	public void mouseClicked(MouseEvent e) {
		Point clicked = e.getPoint();
		/**
		 * Checks if you click one of the puppet changing icons.
		 */
		for (int i = 0; i < puppetNumber; i++) {
			if (ui.getPuppetBounds().get(puppetNames[i]).contains(clicked)) {
				setCurrentPuppet(puppetNames[i], false);
			}
		}
		/**
		 * Checks if you click one of the emote changing icons.
		 */
		int emoteNumber = ui.getEmoteNumber();
		for (int i = 0; i < emoteNumber; i++) {
			if (ui.getEmoteBounds().get(i).contains(clicked)) {
				if (currentEmote == i) {
					setCurrentEmote(0);
				} else {
					setCurrentEmote(i);
				}
			}
		}
		/**
		 * Checks if you click one of the hat changing icons.
		 */
		int hatNumber = ui.getHatNumber();
		for (int i = 0; i < hatNumber; i++) {
			if (ui.getHatBounds().get(i).contains(clicked)) {
				if (showPanel.thisClient.hatsList.contains(i)) {
					hats.remove(new Integer(i));
					showPanel.setCurrentHats(hats);
				} else {
					hats.add(i);
					showPanel.setCurrentHats(hats);
				}
			}
		}
	}

	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	public void componentHidden(ComponentEvent e) {
		// TODO Auto-generated method stub

	}

	public void componentMoved(ComponentEvent e) {
		// TODO Auto-generated method stub

	}

	public void componentResized(ComponentEvent e) {
		JFrame frameTemp = (JFrame) SwingUtilities.getWindowAncestor(this);
		panelWidth = frameTemp.getWidth();
		ui.updateFrame(frameTemp.getWidth());
		repaint();
	}

	public void componentShown(ComponentEvent e) {
		// TODO Auto-generated method stub

	}

}
