package marionettes;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

import java.awt.*;
import java.io.File;
import java.io.IOException;

/**
 * ----------------------------------------------Marionette Mates------------------------------------------------
 * | Original Source by Bakkidza on Reddit                                                                      |
 * |- https://www.reddit.com/r/BuffaloWizards/comments/3g6f43/fuppet_fals_a_puppet_pals_knockoff_that_you_can/  |
 * | Changes and improvements by Ryuski and Endsgamer                                                           |
 * --------------------------------------------------------------------------------------------------------------
 */

@SuppressWarnings("serial")
public class Projection extends JFrame {
	/**
	 * Projection Constructor - Sets up the frame.
	 */
	public Projection() {
		
		setExtendedState(getExtendedState() | JFrame.MAXIMIZED_BOTH);
        setSize(new Dimension(1920,1080));
        setMinimumSize(new Dimension(1280,720));
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
		setTitle("Marrionette Mates");		
		ShowPanel showPanel = new ShowPanel();
		UIPanel uiPanel = showPanel.getUIPanel();
		
		showPanel.addKeyListener(showPanel);
		showPanel.addComponentListener(showPanel);
		showPanel.setFocusable(true);
		uiPanel.addMouseListener(uiPanel);
		uiPanel.addComponentListener(uiPanel);
		uiPanel.setFocusable(true);
		
		this.add(showPanel,BorderLayout.CENTER);
		this.add(uiPanel, BorderLayout.SOUTH);
		
		try {
			this.setIconImage(ImageIO.read(new File("resources/ui/icon.png")));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		uiPanel.setPreferredSize(new Dimension(1920,290));
		setVisible(true);
	}
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		
		new Projection();
		
	}
	
}