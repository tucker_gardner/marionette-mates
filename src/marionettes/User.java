package marionettes;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashMap;


public class User {

	
	// All the images we are using
	ArrayList<BufferedImage> bodiesR;
	ArrayList<BufferedImage> bodiesL;
//	BufferedImage closedRight;
//	BufferedImage closedLeft;
//	BufferedImage ARight;
//	BufferedImage ALeft;
//	BufferedImage IRight;
//	BufferedImage ILeft;
//	BufferedImage ORight;
//	BufferedImage OLeft;
//	BufferedImage ERight;
//	BufferedImage ELeft;
//	BufferedImage browNormalRight;
//	BufferedImage browNormalLeft;
//	BufferedImage browAngryRight;
//	BufferedImage browAngryLeft;
//	BufferedImage browSadRight;
//	BufferedImage browSadLeft;
//	BufferedImage browConfusedRight;
//	BufferedImage browConfusedLeft;
//	BufferedImage browExcitedRight;
//	BufferedImage browExcitedLeft;
	public String username = "";
	public int xCoord; // The x coordinate of the puppet
	public int yCoord; // The y coordinate of the puppet
	public boolean moveRight; // If the puppet is moving to the right
	public boolean moveLeft; // If the puppet is moving to the left
	public int bounce; // How far along in the bounce the puppet is
	public int jumpDist = 10; // How long the puppet is in the air
	public int facing =Expressions.FACING_RIGHT; // Which way the puppet is facing 
	public int moveSpeed = 270; // How many pixels the puppet moves per frame
	public boolean speaking; // Whether or not the puppet is speaking
	public int speakCount; // How many frames it has been since the puppet
							// changed it's mouth shape
	public int speakMax = 12; // About the average number of frames between
								// mouth shape changing
	public int mouthShape =0; // Which shape the puppet's mouth is in (A, I, O,
								// E, closed)
	public int mouthNum; // The number that corresponds to the mouth shape
	
	public int eyebrowNum; //The number that corresponds to the eyebrow position
	public int eyebrowRand;
	public int browShape = 0; // Which eyebrow type we're currently using
	public int speakRand; // The random adjustment to the speakMax timer

	public String currentChar; // Character selection
	public int eyebrowCount; //used for the random eyebrows
	public int eyebrowMax = 120;//about how many frames should happen between 
								//eyebrow transitions when speaking
	public int slotNumber = 0;	//what slot the user is in
	public int prevBrow;		//what the "saved" brow is, so if a 
								//person is speaking, it can transition
								//back at the end
	public int prevMouth;		//same deal as prevBrow
	public int emoteNum = 0;

	public boolean deadbonesStyle;
	public int tiltCount;
	public int tiltRand;
	public double tiltAngle;
	public int tiltMax = 120;
	public int tiltShiftLat;
	public int tiltShiftVert;
	public double yHeight;
	public ArrayList<BufferedImage> hatsRight = new ArrayList<BufferedImage>();
	public ArrayList<BufferedImage> hatsLeft = new ArrayList<BufferedImage>();
	public ArrayList<Integer> hatsList = new ArrayList<Integer>();
	ArrayList<BufferedImage> mouthRight = new ArrayList<BufferedImage>();
	ArrayList<BufferedImage> mouthLeft = new ArrayList<BufferedImage>();
	ArrayList<BufferedImage> browRight = new ArrayList<BufferedImage>();
	ArrayList<BufferedImage> browLeft = new ArrayList<BufferedImage>();
	double tiltAngles[];
	double shiftUps[];
	double shiftLats[];
	public int currentBody;
	@SuppressWarnings("rawtypes")
	public HashMap<Integer, ArrayList<ArrayList>> handsObj;
	public boolean animating;
	public boolean displayHand;
	public int currentHand;
	public int currentHandFrame;
	public int handFrameDelay;
	public boolean prevDisplayHand;
	
	
	public User(String name){
		username = name;

	}
}
