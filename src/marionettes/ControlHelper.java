package marionettes;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;
import javax.swing.JTextPane;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

public class ControlHelper {
    public static void main(String[] args) {
        JFrame frame = new JFrame("Control Helper");
        frame.setVisible(true);
        frame.setSize(new Dimension(600,200));
        frame.setFocusable(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        frame.setLocation(dim.width/2-frame.getSize().width/2, dim.height/2-frame.getSize().height/2);
        final JTextPane textArea = new JTextPane();
        textArea.setEditable(false);
        textArea.setAlignmentX(Component.CENTER_ALIGNMENT);
        textArea.setAlignmentY(Component.CENTER_ALIGNMENT);
        textArea.setFont(new Font("Verdana",Font.BOLD,72));
        StyledDocument doc = textArea.getStyledDocument();
        SimpleAttributeSet center = new SimpleAttributeSet();
        StyleConstants.setAlignment(center, StyleConstants.ALIGN_CENTER);
        doc.setParagraphAttributes(0, doc.getLength(), center, false);
        frame.getContentPane().add(textArea);
        frame.addKeyListener(new KeyListener(){
             
                public void keyPressed(KeyEvent e) {
                   
                    
                }

                
                public void keyTyped(KeyEvent e) {
                    //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }

                
                public void keyReleased(KeyEvent e) {
                	 textArea.setText(KeyEvent.getKeyText(e.getKeyCode()));
                }
        });
        frame.setVisible(true);
    }
}