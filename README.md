# Marionette Mates
***
Marionette Mates is an open source clone of [Puppet Pals]( https://youtu.be/9LbVf2NGRrc?t=119 ). The goal of this project is to attempt to create a version as alike to Puppet Pals as possible to allow groups of people to have authentic an experience when playing [URealms Live]( https://urlivewiki.com/wiki/Unforgotten_Realms_Live ). Marionette Mates is written in Java, and was created from the combined efforts of (people).

# Usage
***
[Java]( https://java.com/en/ ) must be installed on your system to use Marionette Mates.

To begin using Marionette Mates, follow the steps below.

> 1. Download the repository from the [Downloads]( https://bitbucket.org/tutorials/markdowndemo/downloads ) section.
> 2. Exctract the downloaded archive into a location of your choice, it does not matter where it goes. If connecting to another user's server, skip to step 5.
> 3. In the "resources/config.properties" file, choose a port that will be used to connect to other servers and host servers. Default is 9001. (Make sure if you change ports to match the server owners port before trying to connect.)
> 4. Run "Server.jar" to launch the server that clients will connect to. The server has no use other than being informational.
> 5. Run "MarionetteMates.jar" to start the client.
> 6. Enter the ip address of the machine running the Server.
> 7. If a server is found, you will be asked to enter a screen name. Screen names cannot contain spaces and cannot be the same as a client that is already connected.

# Controls
***
* The User Interface allows you to switch the current active character, the emotion, and the currently displayed hats.
* The number keys can be used to switch the current active character.
* Controls can be rebound in the controls.properites file. The controls listed here are default.
* `Q` will face the character left, `E` will face the character right.
+ `A` and `Left Arrow Key` will move the character left if they were already facing left, or make them face left if they were facing right.
 * `D` and the `Right Arrow Key` do the same thing, just in the opposite directions.
 * Characters will move the amount of times the key was pressed.
* `G` and the `Up Arrow Key` will toggle the direction your character is facing.
* `Spacebar` activates the talking animation. The animation will continue as long as spacebar is held down.
* `Y` will set the mouth back to the default.
* `U`, `I`, `O`, and `P` will toggle the mouth between the default and the respective state of the key.
* `H` sets the puppet's eyebrows to the default.
* `J`, `K`, `L`, and `;` will toggle the eyebrows between the default and the respective state of the key.
* `V` and `N` will roll a dice. Up to 5 can be rolled at once.
* '.' will cycle through different bodies for the selected characer.
* `Esc` will close the client.

# Adding New Puppets
***
Included in the source code are two relevent .PSD files to help create characters and icons.  

The guideCharacter folder contains the "guideCharacter.PSD" and the "icons.PSD" files to help you.

> 1. Make a copy of the "guideCharacter" folder in the "resources" folder.
> 2. Rename the folder to the name of your created character.
> 3. Open the "guideCharacter.PSD" file and edit/add all of the bodies, mouths, eyebrows, hats, and etc. that you want for your character.
> 4. Open the "icons.PSD" file and create icons for your character, each of their emotes, and their hats.
> 5. Save the emotes into the "emotes" folder with relevent filenames. Save the character icon in the root of the "character" folder as "icon.png". Save the hats into the "hats" folder with relevent filenames.
> 6. Open the "bodies.json" file and edit the default body's "filePathBody" to your character's body images. Add other bodies accordingly. **MAKE SURE THERE IS AT LEAST ONE**
> 7. Open the "emotes.json" file and edit the emotes filePaths to whichever eyebrows, mouths, and icons apply for whichever emote you are trying to make. Add or remove emotes accordingly.
> 8. Open the "hats.json" file and edit the "hatName" and "filePathHat" for each hat you require. The filepath for the hat icons will be in the "hats" folder and the icon must be named accordingly to the "hatName" you chose here. **MAKE SURE THERE IS AT LEAST ONE**
> 9. Edit the "details.properties" file accordingly for each property you would like to change.
> 10. In the "resources" folder edit the "config.properties" file adding your new character(s) to the "PuppetNames" settings. (Note this must be the same as the folder name.) Also add 1 to the "PuppetNum" setting for each new character you add.